﻿using System;
using VaultSharp;
using VaultSharp.V1.AuthMethods;
using VaultSharp.V1.AuthMethods.Token;
using System.Collections.Generic;
using VaultSharp.V1.Commons;
using System.Threading.Tasks;


// Использована библиотека VaultSharp https://github.com/rajanadar/VaultSharp)
// Документация библиотеки http://rajanadar.github.io/VaultSharp
namespace Bastion.Vault
{
    /// <summary>
    /// Расширение API VaultSharp реализует методы упрощенной работы с хранилищем Vault KeyValue.V2
    /// Для локальной отладки добавьте в настройки VS Run/Configuration/Default переменную окружения ASPNETCORE_ENVIRONMENT=Development и VAULT_TOKEN=токен
    /// В pathAndKey указывать путь без префикса имя_приложения/энвайронмент, т.е. test/v а не имя_приложения/Development/test/v
    /// </summary>
    public static class VaultSharpExtension
    {
        /// <summary>This operator determines whether two Points have the same
        ///    location.</summary>
        /// <param><c>p1</c> is the first Point to be compared.</param>
        /// <param><c>p2</c> is the second Point to be compared.</param>
        /// <returns>True if the Points have the same location and they have
        ///    the exact same type; otherwise, false.</returns>
        /// <seealso cref="Equals"/>
        /// <seealso cref="operator!="/>


        /// <summary>Transforms relative path to absolute one, e.g. test/v to app/Development/test/v.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>relativePath</c> is the relative path.</param>
        /// <returns>absolute Vault path, e.g. test/v to app/Development/test/v.</returns>
        public static String GetAbsPath(this IVaultClient vaultClient, String relativePath)
        {
            return $"{Vault.KVPathPrefix}/{relativePath}";
        }

        /// <summary>Split relative path to base path and key.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>Touple (path, key, absolute path).</returns>
        public static (String, String, String) GetAbsPathWithKey(this IVaultClient vaultClient, String pathAndKey)
        {
            String path = System.IO.Path.GetDirectoryName(pathAndKey);
            String key = System.IO.Path.GetFileName(pathAndKey);

            return (path, key, vaultClient.GetAbsPath(path));
        }

        #region Asynchronous methods

        /// <summary>Asynchronously reads a value from given relative path.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>System.Threading.Tasks.Task&lt;object&gt;.</returns> 
        public static async System.Threading.Tasks.Task<object> GetValueAsync(this IVaultClient vaultClient, String pathAndKey)
        {
            var (path, key, absPath) = vaultClient.GetAbsPathWithKey(pathAndKey);

            // Read DbConnection string, prepend app name and environment to path.
            Dictionary<string, object> dataDictionary = await vaultClient.GetSecretAsync(path);

            if (dataDictionary == null)
                return null;

            return dataDictionary[key];
        }

        /// <summary>Asynchronously reads a typed value from given relative path.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>System.Threading.Tasks.Task&lt;object&gt;.</returns> 
        public static async System.Threading.Tasks.Task<T> GetValueAsync<T>(this IVaultClient vaultClient, String pathAndKey)
        {
            var (path, key, absPath) = vaultClient.GetAbsPathWithKey(pathAndKey);

            // Read DbConnection string, prepend app name and environment to path.
            Dictionary<string, object> dataDictionary = await vaultClient.GetSecretAsync(path);

            if (dataDictionary == null)
                throw new Exception($"Key not found on given path {pathAndKey}");

            return (T)dataDictionary[key];
        }


        /// <summary>Asynchronously writes a value to given relative path. Attention! Please use SetSecretAsync to write multiple key/values as whole Vault Secret</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>System.Threading.Tasks.Task.</returns>
        /// <seealso cref="SetValue"/>
        /// <seealso cref="SetSecretAsync"/>
        public static async System.Threading.Tasks.Task SetValueAsync(this IVaultClient vaultClient, String pathAndKey, object value)
        {
            var (path, key, absPath) = vaultClient.GetAbsPathWithKey(pathAndKey);

            // Read
            Dictionary<string, object> dataDictionary = await vaultClient.GetSecretAsync(path);

            if (dataDictionary != null)
                dataDictionary[key] = value;
            else
                dataDictionary = new Dictionary<string, object>() { { key, value } };

            await vaultClient.V1.Secrets.KeyValue.V2.WriteSecretAsync(absPath, dataDictionary);
        }


        /// <summary>Asynchronously reads a Vault's secret (multiple key/value) from given relative path.
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>path</c> is the relative path, e.g. test.</param>
        /// <returns>System.Threading.Tasks.Task&lt;Dictionary&lt;string, object&gt;&gt;.</returns>
        public static async System.Threading.Tasks.Task<Dictionary<string, object>> GetSecretAsync(this IVaultClient vaultClient, String path)
        {
            var absPath = vaultClient.GetAbsPath(path);

            Secret<SecretData> secret = null;

            try
            {
                secret = await vaultClient.V1.Secrets.KeyValue.V2.ReadSecretAsync(absPath);
            }
            catch (VaultSharp.Core.VaultApiException)
            {
                return null;
            }


            return secret.Data.Data;
        }


        /// <summary>Asynchronously writes a Vault's secret (multiple key/value) to given relative path.
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>path</c> is the relative path, e.g. test.</param>
        /// <param><c>dataDictionary</c> is the key/value dictionary.</param>
        /// <returns>System.Threading.Tasks.Task.</returns>
        public static System.Threading.Tasks.Task SetSecretAsync(this IVaultClient vaultClient, String path, Dictionary<string, object> dataDictionary)
        {
            var absPath = vaultClient.GetAbsPath(path);

            return vaultClient.V1.Secrets.KeyValue.V2.WriteSecretAsync(absPath, dataDictionary);
        }

        #endregion
        #region Synchronous methods

        /// <summary>Synchronously reads a value from given relative path.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>value of type T.</returns> 
        public static T GetValue<T>(this IVaultClient vaultClient, String pathAndKey)
        {
            return (T)vaultClient.GetValue(pathAndKey);
        }

        /// <summary>Synchronously reads a untyped value (object) from given relative path.</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <returns>an object.</returns> 
        public static object GetValue(this IVaultClient vaultClient, String pathAndKey)
        {
            var value = vaultClient.GetValueAsync(pathAndKey).GetAwaiter().GetResult();

            if (value == null)
                throw new Exception($"Key not found on given path {pathAndKey}");

            return value;
        }

        /// <summary>Synchronously writes a value to given relative path. Attention! Please use SetSecret to write multiple key/values as whole Vault Secret</summary>
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>pathAndKey</c> is the relative path, e.g. test/v.</param>
        /// <seealso cref="SetSecret"/>
        public static void SetValue(this IVaultClient vaultClient, String pathAndKey, object value)
        {
            vaultClient.SetValueAsync(pathAndKey, value).GetAwaiter().GetResult();
        }


        /// <summary>Synchronously reads a Vault's secret (multiple key/value) from given relative path.
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>path</c> is the relative path, e.g. test.</param>
        /// <returns>Dictionary&lt;string, object&gt;.</returns>
        public static Dictionary<string, object> GetSecret(this IVaultClient vaultClient, String path)
        {
            return vaultClient.GetSecretAsync(path).GetAwaiter().GetResult();
        }


        /// <summary>Synchronously writes a Vault's secret (multiple key/value) to given relative path.
        /// <param><c>vaultClient</c> is the interface being extended.</param>
        /// <param><c>path</c> is the relative path, e.g. test.</param>
        /// <param><c>dataDictionary</c> is the key/value dictionary.</param>
        public static void SetSecret(this IVaultClient vaultClient, String path, Dictionary<string, object> dataDictionary)
        {
            vaultClient.SetSecretAsync(path, dataDictionary).GetAwaiter().GetResult();
        }

        #endregion

        #region Helpers

        /// <summary>Method <c>IsKubernetized</c> checks what app is running in Kubernetes cluster.</summary>
        /// <returns>True if the app is running in Kubernetes cluster; otherwise, false.</returns>
        public static bool IsKubernetized(this IVaultClient vaultClient)
        {
            return (Environment.GetEnvironmentVariable("KUBERNETES_PORT") == null) ? false : true;
        }

        /// <summary>
        /// Returns DB connection string
        /// </summary>
        /// <returns>DB connection string</returns>
        public static string GetDBConnectionString(this IVaultClient vaultClient)
        {
            return vaultClient.GetValue<string>("DbConnection/string");
        }

        #endregion
    }

}
