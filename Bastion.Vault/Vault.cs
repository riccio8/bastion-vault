﻿using System;
using VaultSharp;
using VaultSharp.V1.AuthMethods;
using VaultSharp.V1.AuthMethods.Token;
using System.Text;


// VaultSharp https://github.com/rajanadar/VaultSharp)
// VaultSharp documentation http://rajanadar.github.io/VaultSharp
namespace Bastion.Vault
{
    /// <summary>
    /// VaultClient wrapper
    /// Supports single Vault server.
    /// For local debugging add valid VAULT_TOKEN to Microsoft Visual Studio Run/Configuration/Default
    /// </summary>
    /// <example>
    /// <code>
    ///   var vaultClient = Vault.CreateClient(string appName, string vaultServerUriWithPort);
    ///
    ///   // after client being initialized by colling CreateClient you may use it like this
    ///   Vault.Client.GetValue("test/v");
    ///
    ///   vaultClient.SetValue("test/v","my secret value");
    ///   string vString = vaultClient.GetValue&lt;string&gt;("test/v");
    ///   var vObject = vaultClient.GetValue("test/v");
    ///   
    ///   var secret = new Dictionary &lt;string, object&gt;()
    ///   {
    ///            { "token", "first token" },
    ///            { "shmoken", "another one" },
    ///   };
    ///
    ///   vaultClient.SetSecret(testPath, secret);
    ///   var secret2 = vaultClient.GetSecret(testPath);
    ///
    ///   // See BastionVaultExamples for more information.
    /// </code>
    /// </example>
    public static class Vault
        {
        static Vault() { }

        //<value>Property <c>VaultServerUriWithPort</c> represents the URI of Vault server, e.g. https://vault.example.com.</value>
        public static string ServerUriWithPort { get; set; } = "";

        /// <value>Property <c>VaultServerUriWithPort</c> represents the name of an application, using as part of path</value>
        public static string AppName { get; set; } = "";

        /// <value>Property <c>DeploymentEnvironment</c> represents the running environment name, using as part of path, "Development" by default</value>
        public static string DeploymentEnvironment
        {
            get
            {
                var deploymentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                if (deploymentEnvironment == null)
                    deploymentEnvironment = "Development";

                return deploymentEnvironment;
            }
        }


        static VaultClientSettings clientSettings = null;

        /// <value>Property <c>VaultClientSettings_</c> stores Vault settings</value>
        public static VaultClientSettings ClientSettings 
        {
            get
            {
                // Get vault token
                var myVaultToken = Environment.GetEnvironmentVariable("VAULT_TOKEN");

                StringBuilder errors = new StringBuilder();

                if (ServerUriWithPort == "")
                    errors.AppendLine("VaultServerUriWithPort variable");

                if (AppName == "")
                    errors.AppendLine("AppName variable");

                if (myVaultToken == null)
                    errors.AppendLine("Environment variable VAULT_TOKEN=<your_access_token>");

                if (errors.Length > 0)
                    throw new Exception("Please set :" + errors.ToString());


                IAuthMethodInfo authMethod = new TokenAuthMethodInfo(myVaultToken);

                // Initialize settings. You can also set proxies, custom delegates etc. here.
                clientSettings = new VaultClientSettings(ServerUriWithPort, authMethod);

                return clientSettings;
            }
            set
            {
                clientSettings = value;
            }
        }


        static IVaultClient client;

        /// <value>Property <c>Client</c> creates and returns IVaultClient, reusing one instance</value>
        public static IVaultClient Client
        {
            get
            {
                if (client == null)
                    client = new VaultClient(ClientSettings);

                return client;
            }
        }

        /// <summary>This method creates and returns IVaultClient.</summary>
        /// <param><c>AppName</c> represents the name of an application, using as part of path.</param>
        /// <param><c>VaultServerUriWithPort</c> the URI of Vault server, e.g. https://vault.example.com</param>
        /// <returns>IVaultClient</returns>
        public static IVaultClient CreateClient(string appName, string vaultServerUriWithPort)
        {
            ServerUriWithPort = vaultServerUriWithPort;
            AppName = appName;

            return Client;
        }

        /// <value>Property <c>KVPathPrefix</c> represents Vault base path prefix, &lt;name_of_app&gt;/&lt;name_of_environment&gt;</value>
        public static string KVPathPrefix
        {
            get
            {
                return $"{Vault.AppName}/{Vault.DeploymentEnvironment}";
            }
        }

    }


}
