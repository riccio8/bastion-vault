﻿using System;
using System.Collections.Generic;
using VaultSharp;
using Bastion.Vault;


// This example uses Bastion.Vault library which extends VaultSharp.
// Add VAULT_TOKEN environment variable to VS Run/Configuration/Default
// More about VaultSharp:
// VaultSharp https://github.com/rajanadar/VaultSharp)
// VaultSharp documentation http://rajanadar.github.io/VaultSharp
namespace VaultExample
{

    class Program
    {
        public static IVaultClient vaultClient;

        public static string testPath = "test/vault3";

        // Basic methods test
        static void testVaultBasic()
        {
            Console.WriteLine("Test basic synchronous methods");

            Console.WriteLine("Save a string value for given key");
            vaultClient.SetValue(testPath+"/token", "token");

            Console.WriteLine("Read the string value for given key");
            Console.WriteLine(vaultClient.GetValue<String>(testPath+"/token"));

            Console.WriteLine("Save an integer value for given key");
            vaultClient.SetValue(testPath + "/number", 1);

            Console.WriteLine("Read the integer value for given key");
            Console.WriteLine(vaultClient.GetValue<long>(testPath + "/number").ToString());
        }

        // Syncronous methods test
        static void testVaultExtendedSync()
        {
            Console.WriteLine("Save a value for given key");
            vaultClient.SetValue(testPath+"/token", "token");

            Console.WriteLine("Read a value for given key");
            Console.WriteLine(vaultClient.GetValue(testPath+"/token"));
            Console.WriteLine(vaultClient.GetValue<String>(testPath+"/token"));


            Console.WriteLine("Save a secret");
            var secret = new Dictionary<string, object>()
            {
                { "token", "asasdasdasdasd" },
                { "shmoken", "asasdasdasdasd" },
            };

            vaultClient.SetSecret(testPath, secret);

            Console.WriteLine("Read the whole secret");
            var secret2 = vaultClient.GetSecret(testPath);

            foreach (KeyValuePair<string, object> item in secret2)
            {
                Console.WriteLine($"Key: {item.Key} , Value : {item.Value.ToString()}");
            }
        }

        // Asynronous methods test
        static async System.Threading.Tasks.Task testVaultExtendedAsync()
        {
            Console.WriteLine("Save a value for given key");
            await vaultClient.SetValueAsync(testPath+"/token", "token");

            Console.WriteLine("Read a value for given key");
            Console.WriteLine(await vaultClient.GetValueAsync(testPath+"/token"));
            Console.WriteLine((string)await vaultClient.GetValueAsync(testPath+"/token"));


            Console.WriteLine("Save a secret");
            var secret = new Dictionary<string, object>()
            {
                { "token", "asasdasdasdasd" },
                { "shmoken", "asasdasdasdasd" },
            };

            await vaultClient.SetSecretAsync(testPath, secret);

            Console.WriteLine("Read the whole secret");
            var secret2 = await vaultClient.GetSecretAsync(testPath);

            foreach (KeyValuePair<string, object> item in secret2)
            {
                Console.WriteLine($"Key: {item.Key} , Value : {item.Value.ToString()}");
            }
        }


        static async System.Threading.Tasks.Task Main(string[] args)
        {
            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            vaultClient = Vault.CreateClient("migom", "https://vault.migom.com");

            // Basic methods test
            testVaultBasic();

            // Syncronous methods test
            testVaultExtendedSync();

            // Asynronous methods test
            await testVaultExtendedAsync();
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }
    }
}
