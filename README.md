# Vault

Библиоткека реализует упрощенные методы для работы с HashiCorp Vault KeyValue engine V2, расширяя существующий интерфейс IVaultClient. См. документацию методов в коде.

Для локальной отладки добавьте в настройки VS Run/Configuration/Default переменную окружения ASPNETCORE_ENVIRONMENT=Development и VAULT_TOKEN=токен.
В папке Example вы можете найти проект-пример использования библиотеки, подключенной как пакет NuGet.

На [сайте](https://learn.hashicorp.com/vault) Hashicorp Vault опубликована [документация](https://learn.hashicorp.com/vault), подробно объясняющая нюансы использования этой библиотеки. 

## Версии KV хранилищ
На данный момент существует две версии хранилища ключ/значение:
- V1 не поддерживает историю изменений секрета.
- V2 поддерживает хранение истории.

Мы будем использовать только хранилище секретов kv версии 2.
Вызовы API для версии V2 отличаются от вызовов V1: vaultClient.V1.Secrets.KeyValue.**V2** вместо vaultClient.V1.Secrets.KeyValue.**V1**

Подробнее о [V1/V2](https://learn.hashicorp.com/vault/secrets-management/sm-versioned-kv)

Для работы с Vault из C# используется библиотека [VaultSharp](https://github.com/rajanadar/VaultSharp), ее документация доступна по [ссылке](http://rajanadar.github.io/VaultSharp/).


## Как опубликовать библиотеку в репозиторий NuGet

```bash
nuget pack -properties Configuration=Release 

# you may set api key permanently
nuget setApiKey your_API_key

nuget push -Source https://api.nuget.org/v3/index.json  Bastion.Vault.0.0.1.nupkg
# or
dotnet nuget push Bastion.Vault.0.0.1.nupkg -s https://api.nuget.org/v3/index.json

# Without nuget setApiKey:

nuget push -Source https://api.nuget.org/v3/index.json -ApiKey your_API_key Bastion.Vault.0.0.1.nupkg
# or
dotnet nuget push Bastion.Vault.0.0.1.nupkg -k your_API_key -s https://api.nuget.org/v3/index.json
```

Больше информации о [NuGet](https://docs.microsoft.com/en-us/nuget/nuget-org/publish-a-package)